# LeapYears

This project is answering the following code kata: [Leap Years](http://codingdojo.org/kata/LeapYears/).

It is an Android application made using Kotlin. It features a view on which the user can enter a year and check if it's a leap year.

Identifier: `2019_DEV_088`

## How to build and run
Open the project using Android Studio and run on device or emulator.
