package com.kotlin.leapyears.utils

fun Int.isLeapYear(): Boolean {
    if (this.rem(4) > 0) return false
    if (this.rem(100) > 0) return true
    if (this.rem(400) > 0) return false
    return true
}