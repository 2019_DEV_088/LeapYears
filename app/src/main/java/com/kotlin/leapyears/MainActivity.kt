package com.kotlin.leapyears

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.kotlin.leapyears.utils.isLeapYear
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.NumberFormatException

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        checkLeap.setOnClickListener {
            try {
                val year = yearInput.text.toString().toInt()
                val isLeapYear = year.isLeapYear()
                leapResult.text = getString(if (isLeapYear) R.string.is_leap_year else R.string.is_not_leap_year, year)
            } catch(e: NumberFormatException) {
                leapResult.text = getString(R.string.leap_error)
            }
        }
    }
}
