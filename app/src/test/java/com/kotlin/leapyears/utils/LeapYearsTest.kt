package com.kotlin.leapyears.utils

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class LeapYearsTest {

    @Test fun yearIsALeapYear() {
        assertTrue(2000.isLeapYear())
        assertTrue(2004.isLeapYear())
        assertTrue(2008.isLeapYear())
        assertTrue(2012.isLeapYear())
        assertTrue(2016.isLeapYear())
        assertTrue(2020.isLeapYear())
        assertTrue(2024.isLeapYear())
        assertTrue(2028.isLeapYear())
        assertTrue(2032.isLeapYear())
        assertTrue(2036.isLeapYear())
        assertTrue(2040.isLeapYear())
        assertTrue(2060.isLeapYear())
        assertTrue(2072.isLeapYear())
        assertTrue(2080.isLeapYear())
}

    @Test fun yearIsNotALeapYear() {
        assertFalse(1700.isLeapYear())
        assertFalse(1800.isLeapYear())
        assertFalse(1900.isLeapYear())
        assertFalse(2017.isLeapYear())
        assertFalse(2018.isLeapYear())
        assertFalse(2019.isLeapYear())
        assertFalse(2030.isLeapYear())
        assertFalse(2100.isLeapYear())
    }
}